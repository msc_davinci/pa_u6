package org.davinci.aspectouno;

import java.util.LinkedList;

public class ListaNombres {
    private static LinkedList<String> lista = new LinkedList<String>();

    private static final String nombres[] = { "Gilberto", "Laura",
            "Fernando", "Paola", "Gilberto", "Silvia" };

    public static void inserta(String[] nombres) {
        // recorrido del array nombres
        System.out.println("La lista de nombres es: ");
        for (String s : nombres) {
            // lo añade si no está ya incluido
            if (!lista.contains(s)) {
                lista.addLast(s);
                System.out.printf("%s, ", s);
            }
        }
    }

    private static void buscar(String nombre){
        System.out.printf(lista.contains(nombre)?"\nSe encontró el nombre %s en la lista":"\nNo se encontró el nombre %s en la lista", nombre);
    }

    public static void main(String[] args) {
        inserta(nombres);

        //Test 1
        buscar("Laura");

        //Test 2
        buscar("Mireya");

        //Test 3
        buscar("Gilberto");

        //Test 4
        buscar("Luis");
    }
}
